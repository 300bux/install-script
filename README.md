# How to use:

### Downloading
`curl -O https://gitlab.com/300bux/install-script/-/raw/master/install.sh`

### Running on Linux
```
sudo chmod +x install.sh
./install.sh
```

### Running on OpenBSD
```
doas chmod +x install.sh
bash install.sh
```

