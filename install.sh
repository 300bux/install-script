#!/bin/sh

welcomeMessage() {
    echo ""
    echo " ______________________________________________________________________________"
    echo "/                                                                              \\"
    echo "|     ________  __________ __ _______   __   _____ __    ___ _    _____________ |"
    echo "|    / ____/ / / / ____/ //_// ____/ | / /  / ___// /   /   | |  / / ____/ ___/ |"
    echo "|   / /_  / / / / /   / ,<  / __/ /  |/ /   \\__ \\/ /   / /| | | / / __/  \\__ \\  |"
    echo "|  / __/ / /_/ / /___/ /| |/ /___/ /|  /   ___/ / /___/ ___ | |/ / /___ ___/ /  |"
    echo "| /_/    \\____/\\____/_/ |_/_____/_/ |_/   /____/_____/_/  |_|___/_____//____/   |"
    echo "|                                                                               |"
    echo "|                           GET YOUR ASSES BACK HERE                            |"
    echo "\\                                                                               /"
    echo " -------------------------------------------------------------------------------"
    echo "        \\   ^__^"
    echo "         \\  (oo)\\_______"
    echo "            (__)\\       )\\/\\"
    echo "                ||----w |"
    echo "                ||     ||"
    echo ""
    echo "Warning, script will delete your home ~/ folder"
    echo ""
}

selectGraphics() {
    while true; do
        echo "1) Intel"
        echo "2) Nvidia (FOSS)"
        echo "3) Nvidia (Proprietary)"
        echo "4) AMD"
        echo "5) VirtualBox"
        echo "6) Other/Not sure"
        echo "7) None (Don't install any drivers)"
        read -p "Graphics driver to install: " an
        case $an in
            [1]* ) GPU="xf86-video-intel";   break;;
            [2]* ) GPU="xf86-video-nouveau"; break;;
            [3]* ) GPU="nvidia";             break;;
            [4]* ) GPU="xf86-video-amdgpu";  break;;
            [5]* ) GPU="xf86-video-fbdev";   break;;
            [6]* ) GPU="xf86-video-fbdev";   break;;
            [7]* ) GPU=false;                break;;
            * ) 
                echo ""
                echo "Please select from 1-7"
                echo ""
                ;;
        esac
    done
}

selectWifi() {
    while true; do
        read -p "Setup WiFi when done? [Y/n] " wan
        case $wan in
            [yY][eE][sS]|[yY] )
                INSTALL_WIFI=true; break;;
            [nN][oO]|[nN] )
                INSTALL_WIFI=false; break;;
            * )
                echo ""
                echo "Invalid input..."
                echo ""
                ;;
        esac
    done
}

voidPackagesInstall() {
    $SU xbps-install -Su &&
    $SU xbps-install void-repo-debug void-repo-multilib &&
    $SU xbps-install -Su &&

    $SU xbps-install \
    neovim \
    NetworkManager \
    alsa-oss \
    alsa-plugins \
    alsa-utils \
    base-system \
    betterlockscreen \
    bleachbit \
    bspwm \
    cava \
    cmatrix \
    cryptsetup \
    curl \
    dunst \
    dvtm \
    ffmpeg \
    firefox \
    flameshot \
    gcc \
    grub-i386-efi \
    grub-x86_64-efi \
    harfbuzz \
    harfbuzz-devel \
    htop \
    kakoune \
    lazygit \
    libXft-dbg \
    libXft-devel \
    libXinerama-devel \
    libXrandr-devel \
    libgraphicsmagick \
    libsecret \
    lvm2 \
    lxappearance \
    lynx \
    make \
    mesa \
    mpc \
    mpd \
    mpv \
    ncmpcpp \
    neofetch \
    nodejs \
    nnn \
    pamixer \
    pavucontrol \
    pcmanfm \
    picom \
    polybar \
    pkg-config \
    pulsemixer \
    python \
    python-lxml \
    python-pip \
    pywal \
    rofi \
    sndio \
    srm \
    pipe-viewer \
    sxhkd \
    tree \
    tty-clock \
    ufw \
    VeraCrypt \
    xclip \
    xcursor-vanilla-dmz \
    xcursor-vanilla-dmz-aa \
    xcursorgen \
    xdg-user-dirs \
    xinit \
    xorg \
    xorg-fonts \
    xrdb \
    xsetroot \
    youtube-dl \
    zathura \
    zathura-pdf-poppler \
    zsh &&
    if [ $GPU != false ]; then $SU xbps-install $GPU; fi
}

yayInstall() {
    # Update and install git
    $SU pacman -Syu &&
    $SU pacman -S git base-devel &&
   
    # Clone and install yay
    mkdir -p ~/apps/ &&
    cd apps/ &&
    git clone https://aur.archlinux.org/yay.git &&
    cd yay/ &&
    makepkg -si
}

archPackagesInstall() {
    yay &&
    yay -S \
    bandcamp-dl-git \
    betterlockscreen-git \
    bleachbit \
    blender \
    bucklespring \
    bzip2 \
    cava \
    cmatrix \
    curl \
    dash \
    dashbinsh \
    dhcpcd \
    dunst \
    dvtm \
    editorconfig-core-c \
    figlet \
    flameshot \
    gcc \
    gimp \
    git \
    gzip \
    htop \
    inkscape \
    kakoune \
    kakoune.cr-git \
    kak-lsp \
    kdenlive \
    krita \
    lazygit \
    librewolf-bin \
    libxft-bgra \
    lolcat \
    lxappearance-gtk3 \
    make \
    mat2 \
    mpc \
    mpd \
    mpv \
    mumble \
    mupdf \
    ncmpcpp \
    neofetch \
    neovim \
    networkmanager \
    newsboat \
    nnn \
    nodejs \
    npm \
    obs-studio \
    opentoonz \
    otf-bebas-neue-git \
    otf-source-han-code-jp \
    p7zip \
    pamixer \
    pavucontrol \
    pcmanfm \
    picom \
    pipe-viewer-git \
    plover-git \
    pulsemixer \
    pipewire \
    pipewire-alsa \
    pipewire-jack \
    pipewire-pulse \
    python \
    python-lxml \
    python-pip \
    python-pywal \
    qt5ct \
    rofi-emoji \
    srm \
    sudo \
    tar \
    tmux \
    tor-browser \
    transmission-cli \
    tree \
    tremc-git \
    ttf-exo-2 \
    ttf-ms-fonts \
    ttf-roboto-mono \
    ttf-twemoji \
    tty-clock \
    unrar \
    unzip \
    urlscan \
    veracrypt \
    weechat \
    xclip \
    pavucontrol \
    xdg-user-dirs \
    xf86-input-wacom \
    xorg-server \
    xorg-apps \
    xorg-xinit \
    xwallpaper \
    youtube-dl \
    zathura \
    zathura-pdf-poppler \
    zls-git \
    zsh \
    zstd &&
    if [ $GPU != false ]; then yay -S $GPU; fi &&
    sudo pacman -R feh
}

openbsdPackageInstall() {
    $SU pkg_add -u &&

    $SU pkg_add \
    neovim \
    # alsa-oss \
    # alsa-plugins \
    # alsa-utils \
    # base-system \
    #betterlockscreen \
    #bleachbit \
    bspwm \
    #cava \
    cmatrix \
    #cryptsetup \
    curl \
    dunst \
    dvtm \
    ffmpeg \
    firefox \
    flameshot \
    gcc \
    #grub-i386-efi \
    #grub-x86_64-efi \
    harfbuzz \
    #harfbuzz-devel \
    htop \
    kakoune \
    #lazygit \
    #libXft-dbg \
    #libXft-devel \
    #libXinerama-devel \
    #libXrandr-devel \
    libgraphicsmagick \
    libsecret \
    #lvm2 \
    lxappearance \
    lynx \
    #make \
    #mesa \
    mpc \
    mpd \
    mpv \
    ncmpcpp \
    neofetch \
    node \
    nnn \
    #pamixer \
    pavucontrol \
    pcmanfm \
    picom \
    polybar \
    #pkg-config \
    pulseaudio \
    #pulsemixer \
    python \
    python-lxml \
    #python-pip \
    #pywal \
    rofi \
    #sndio \
    #srm \
    #straw-viewer \
    sxhkd \
    #tty-clock \
    #ufw \
    #VeraCrypt \
    xclip \
    xcursor-vanilla-dmz \
    xcursor-vanilla-dmz-aa \
    xcursorgen \
    xdg-user-dirs \
    #xinit \
    #xorg \
    #xorg-fonts \
    #xrdb \
    #xsetroot \
    youtube-dl \
    zathura \
    zathura-pdf-poppler \
    zsh &&
    if [ $GPU != false || $GPU != "nvidia" ]; then $SU xbps-install $GPU; fi
}

dotsInstall() {
    cd &&
    git clone $GIT_REPO_OWNER_URL/dotfiles &&
    cd dotfiles/ &&
    sed -i 's/changeme/'"$USER"'/g' .config/flameshot/flameshot.conf &&
    sed -i 's/changeme/'"$USER"'/g' .config/flameshot/flameshot.ini &&
    rm -rf \
        .git \
        README.md \
        apps/test \
        dl/test \
        dox/test \
        media/audio/test \
        media/vids/test \
        media/pix/ss/test \
        media/pix/mpv/test \
        media/pix/clips/test \
        ~/.local &&
    mv -n * .* ~/ &&
    chsh -s /bin/zsh &&
    mkdir ~/.cache/zsh/ &&
    touch ~/.cache/zsh/history &&
    xdg-user-dirs-update &&
    rm -rf ~/dotfiles/
}

fontInstall() {
    $SU mv ~/fonts/Inter/* /usr/share/fonts/TTF/ &&
    $SU mv ~/fonts/JetBrains/* /usr/share/fonts/TTF/ &&
    rm -rf ~/fonts/
}

sucklessInstall() {
    # Clone suckless
    cd ~/apps/ &&
    git clone $GIT_REPO_OWNER_URL/st &&
    git clone $GIT_REPO_OWNER_URL/dwm &&
    git clone $GIT_REPO_OWNER_URL/dwmblocks &&
    git clone $GIT_REPO_OWNER_URL/slock &&
    git clone $GIT_REPO_OWNER_URL/dmenu &&
    git clone https://git.mobley.ne02ptzero.me/~louis/sxiv &&

    # Compile suckless
    cd ~/apps/st && $SU make install clean &&
    cd ~/apps/dwm && $SU make install clean &&
    cd ~/apps/dwmblocks && $SU make install clean &&
    cd ~/apps/dmenu && $SU make install clean &&
    cd ~/apps/sxiv && $SU make install clean &&

    # Setup Lockscreen
    cd ~/apps/slock &&
    sed -i 's/changeme/'"$USER"'/g' config.h &&
    $SU make install clean &&
    cd
}

setTheme() {
    ~/.local/bin/wp ~/media/pix/walls/comfy.jpg
}

npmInstall() {
    rm -rf ~/.npm-global &&
    mkdir ~/.npm-global &&
    npm config set prefix "~/.npm-global" &&
    source ~/.zprofile &&
    npm i -g nodemon typescript
}

neovimInstall() {
    cd ~/.config/ &&
    git clone $GIT_REPO_OWNER_URL/nvim &&
    npm i -g neovim emmet-cli &&
    pip install neovim pylint &&
    echo "Open nvim and run :PlugInstall and :CocInstall"
}

kakouneInstall() {
    cd ~/.config/kak/ &&
    mkdir plugins/ &&
    cd plugins/ &&
    git clone https://github.com/robertmeta/plug.kak &&
    npm i -g eslint eslint-formatter-kakoune typescript typescript-language-server prettier &&
    echo "Open kak and run :plug-install" &&
    cd
}

# Will add for arch later
voidWifiInstall() {
    $SU killall -q dhcpcd &&
    $SU rm -rf /var/service/dhcpcd &&
    $SU ln -s /etc/sv/dbus /var/service/ &&
    $SU ln -s /etc/sv/NetworkManager /var/service/ &&
    $SU NetworkManager &&
    $SU nmtui
}

doneMessage() {
    echo ""
    echo "All done :D"
    echo "Please reboot then use startx to start the graphical environment"
    echo ""
}

#################################################
# This is where the good stuff actually happens #
#################################################

DISTRO="$(lsb_release -is)"
UNAME="$(uname)"
GIT_REPO_OWNER_URL="https://gitlab.com/300bux"
SU="sudo"

if [ $UNAME = "OpenBSD" ]
then
    DISTRO="OpenBSD"
    SU="doas"
fi

echo "Detected ${DISTRO} as Distro"

case $DISTRO in 
    Arch)
        welcomeMessage
        selectGraphics
        yayInstall
        archPackagesInstall
        dotsInstall
        fontInstall
        sucklessInstall
        setTheme
        npmInstall
        neovimInstall
        kakouneInstall
        doneMessage
        ;;

    Debian)
        echo "Install for this distro isn't ready yet, come back later!"
        exit 1
        ;;

    OpenBSD)
        echo "Warning this is an alpha test!!!"
        welcomeMessage
        selectGraphics
        openbsdPackageInstall
        dotsInstall
        fontInstall
        sucklessInstall
        setTheme
        npmInstall
        neovimInstall
        kakouneInstall
        doneMessage
        ;;

    VoidLinux)
        welcomeMessage
        selectGraphics
        selectWifi
        voidPackagesInstall
        dotsInstall
        fontInstall
        sucklessInstall
        setTheme
        npmInstall
        neovimInstall
        kakouneInstall
        $INSTALL_WIFI && voidWifiInstall
        doneMessage
        ;;

    *)
        echo "Distro invalid or not detected!"
        echo "Supported distros:"
        echo "Arch"
        echo "Debian"
        echo "OpenBSD"
        echo "VoidLinux"
        exit 1
        ;;
esac

